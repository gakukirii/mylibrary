﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VectorField : MonoBehaviour {

	private int fieldWidth = 10;
	private int fieldHeight = 10;
	private int fieldSize;
	private int fieldDepth = 10;
	private List<Vector3> pt;

	private LineTest line;
	private Material m_LineMaterial;
	private Vector3[,,] dir; 
	private Vector3[,,] _to;

	public GameObject go;
	public bool isCreate;

	// Use this for initialization
	void Start () {
		line = new LineTest();
		dir = new Vector3[fieldWidth,fieldHeight,fieldDepth];
		_to = new Vector3[fieldWidth,fieldHeight,fieldDepth];
		isCreate = true;
		createDir();


	}
	
	// Update is called once per frame
	void Update () {

	}

	void Awake()
	{
		m_LineMaterial = createLineMaterial( Color.white );
	}

	private Material	createLineMaterial( Color color )
	{
		return new Material(
			"Shader \"Lines/Background\" { Properties { _Color (\"Main Color\", Color) = ("+color.r+","+color.g+","+color.b+","+color.a+") } SubShader { Pass { ZWrite on Blend SrcAlpha OneMinusSrcAlpha Colormask RGBA Lighting Off Offset 1, 1 Color[_Color] }}}"
			);
	}
	
	private void drawLine(Vector3 LineStartPos, Vector3 LineFinishPos){
		Vector3 vPos    = Vector3.zero;
		Quaternion qRot = Quaternion.identity;
		Matrix4x4 mtx   = Matrix4x4.TRS( vPos, qRot, Vector3.one );
		
		
		GL.PushMatrix();
		{
			GL.MultMatrix( mtx );
			
			m_LineMaterial.color = Color.yellow;
			m_LineMaterial.SetPass( 0 );
			
			GL.Begin( GL.LINES );
			{
				GL.Vertex( LineStartPos);
				GL.Vertex( LineFinishPos);
			}
			GL.End();
		}
		GL.PopMatrix();
	}

	public void setupField(int innerW, int innerH, int innerD){
		fieldWidth = innerW;
		fieldHeight = innerH;
		fieldDepth = innerD;

		fieldSize = fieldWidth * fieldHeight * fieldDepth;
		for(int i=0; i<fieldSize; i++){
			pt.Add(new Vector3(0, 0, 0));
		}
	}

	public void clearField(){

	}


	public void randomField(float scale){
		for(int i=0; i<fieldSize; i++){
			float x = Mathf.PerlinNoise(Time.deltaTime, i);//Random.Range(-1.0f, 1.0f);
			float y = Mathf.PerlinNoise(Time.deltaTime, i);//Random.Range(-1.0f, 1.0f);
			float z = Mathf.PerlinNoise(Time.deltaTime, i);//Random.Range(-1.0f, 1.0f);
			pt.Add(new Vector3(x, y, z));
		}
	}

	private void OnRenderObject(){

//		for(int w=0; w<fieldWidth; w++){
//			for(int h=0; h<fieldHeight; h++){
//				for(int d=0; d<fieldDepth; d++){
//					Vector3 _center = new Vector3(w+0.5f, h+0.5f, d+0.5f);
//					Vector3 _to = new Vector3(Random.Range(0.0f, 1.0f)+w, Random.Range(0.0f, 1.0f)+h, Random.Range(0.0f, 1.0f)+d);
//					drawLine(_center, _to);
//					dir[w,h,d] = _center - _to;
//				}
//			}
//		}
//		if(isCreate){
			createVector();
//			isCreate = false;
//		}

		int x = (int)go.transform.position.x;
		int y = (int)go.transform.position.y;
		int z = (int)go.transform.position.z;
		Debug.Log(dir[x, y, z]);

	}

	private void createVector(){

		for(int w=0; w<fieldWidth; w++){
			for(int h=0; h<fieldHeight; h++){
				for(int d=0; d<fieldDepth; d++){
					Vector3 _center = new Vector3(w+0.5f, h+0.5f, d+0.5f);
					drawLine(_center, _to[w,h,d]);
					dir[w,h,d] = _center - _to[w,h,d];
				}
			}
		}
	}

	private void createDir(){
		for(int w=0; w<fieldWidth; w++){
			for(int h=0; h<fieldHeight; h++){
				for(int d=0; d<fieldDepth; d++){
					_to[w,h,d] = new Vector3(Mathf.PerlinNoise(Random.Range(-1.0f, 1.0f), w)+w, Mathf.PerlinNoise(Random.Range(-1.0f, 1.0f), h)+h, Mathf.PerlinNoise(Random.Range(-1.0f, 1.0f), d)+d);
				}
			}
		}
	}

	public Vector3[,,] getDirVector(){
		return dir;
	}
}
