﻿
float _norm(float value, float low, float hight){
	return(value-low)/(hight-low);
}

float _Lerp(float sourceValue1, float sourceValue2, float amount){
	return sourceValue1 + (sourceValue2 - sourceValue1) * amount;
}
	
float Map(float value, float low1, float hight1, float low2, float hight2){
	return _Lerp(low2, hight2, _norm(value, low1, hight1));
}